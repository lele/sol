.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   dom 26 apr 2020, 09:20:29
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2020 Lele Gaifax
..

====================================
 :mod:`sol.models.boards` -- Boards
====================================

.. automodule:: sol.models.board

.. autoclass:: Board
   :members:
