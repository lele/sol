.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   mer 29 gen 2014 09:44:51 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2014, 2020 Lele Gaifax
..

============
 Appendixes
============

.. toctree::

   rules
   solitude
   dedication
   development
   privacy
