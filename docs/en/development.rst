.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   lun 31 mar 2014 19:37:57 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2014, 2018, 2020 Lele Gaifax
..

=============
 Development
=============

SoL development is carried on in a git__ repository on GitLab__, that is automatically mirrored
on Bitbucket__.

You can visit the `activity`__ page to see what's happened recently.

If you are a developer, you are more than welcome to `fork it`__ and adapt or improve it to fit
your needs, and I will happily integrate back the changes you may contribute.

Alternatively you can translate it into another language, using the `online Weblate service`__.

There is also some `technical documentation`__ automatically extracted from the sources.

__ http://git-scm.com/
__ https://gitlab.com/metapensiero/SoL
__ https://bitbucket.org/lele/sol/
__ https://gitlab.com/metapensiero/SoL/activity
__ https://docs.gitlab.com/ee/workflow/forking_workflow.html
__ https://hosted.weblate.org/projects/sol/
__ ../index.html


Donations
=========

Since the beginning\ [#]_ I sustained all the maintenance costs of SoL, both in terms of study
and development of the software, and the hosting of the service, usable and browsable for free.

Since some time `ECC`__ sponsors the development of SoL, bearing part of the server maintenance
costs.

If you use SoL to organize tournaments, in particular of national relevance, please
**consider** the opportunity of making a donation contributing to its development and
maintenance, so that the service may be keep going.

.. [#] I started writing ``Scarry`` in October 1999, while ``SoL`` saw the light just under ten
       years later!

__ http://www.european-carrom-confederation.com/pageID_1469902.html


Liberapay
---------

.. raw:: html

   <script src="https://liberapay.com/lele/widgets/button.js"></script>
   <noscript>
     <a href="https://liberapay.com/lele/donate">
       <img alt="Donate using Liberapay"
            src="https://liberapay.com/assets/widgets/donate.svg">
     </a>
   </noscript>


PayPal
------

.. raw:: html

   <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
     <input type="hidden" name="cmd" value="_s-xclick" />
     <input type="hidden" name="hosted_button_id" value="WEWAYTXBYECKL" />
     <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif"
            border="0" name="submit" title="PayPal - The safer, easier way to pay online!"
            alt="Donate with PayPal button" />
     <img alt="" border="0" src="https://www.paypal.com/en_IT/i/scr/pixel.gif" width="1"
          height="1" />
   </form>
