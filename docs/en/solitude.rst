.. -*- coding: utf-8 -*-
.. :Project:   SoL -- SoLitude variant
.. :Created:   dom 5 apr 2020, 09:58:42
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2020, 2021 Lele Gaifax
..

=========================
 Solitude Carrom Playing
=========================

During the first months of the year 2020 all humanity faced an impetuous `healthcare
emergency`__: probably for the first time in history, all human activities in all the populated
areas of the planet have been overwhelmed, and while I'm writing nobody knows yet about how
long it will last, let alone what the social and economic consequences will be in the
long-term.

My thoughts go out to the families of the hundreds of thousands of victims and at the same time
to all the people who face the pandemic by risking their own health for the sake of the others
and jeopardizing the life of their loved ones.

__ https://en.wikipedia.org/wiki/2019%E2%80%9320_coronavirus_pandemic


.. _corona carrom:

Corona Carrom
=============

Since basically the only defense we can apply is that of the `social distancing`__ and that all
group play activities have been suspended indefinitely, Carrom players have adapted, inventing
different competitive modes.

__ https://en.wikipedia.org/wiki/2019%E2%80%9320_coronavirus_pandemic#Social_distancing

**Stay at home and practice!**


Rules of the game common to all systems
---------------------------------------

Your match must be recorded **live** on the appropriate ``Facebook`` or other social media
group:

- go to your appointed group
- place your phone in a horizontal position and make sure your match is recorded horizontally
- click on ``Live Video`` in the top of the page
- click ``Start Video`` when you are ready to play

1. Arrange the coins on the carrom board. During the game you must pocket all the black and
   white coins according to the rules of your specific tournament

2. Break and start to count the number of strokes that that you fail to pocket (miss). Count
   your misses with a counter, beans or any other system visible to all viewers

3. Follow the official rule of *Due* and *Double Due*:

   *Due*
     take out 1 coin and place in the circle on the opposite side from you and add 1 to the
     count

   *Double Due*
     take out 2 coins and place them horizontally touching each other on the opposite side of
     the circle without adding the count

4. First board finishes when you pocket all 18 coins and the queen: note the count of misses
   you made to finish the first board somewhere visible to all

5. Repeat the above test for the prescribed number boards

6. Eventually, visit the SoL page at the URL you received by email, insert the counters in the
   form and confirm it
