.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   dom 09 nov 2008 19:18:57 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2009, 2010, 2014 Lele Gaifax
..

========
 Dedica
========

Per la seconda volta mi trovo a *usare* il Carrom come terapia, per
tener botta a momenti tristi. Funziona benissimo!

SoL è parzialmente dovuto a Bepi Mondà, miope proprietario di una
software house locale, che dopo dieci anni di collaborazione e
dedizione da parte mia ha unilateralmente interrotto ogni rapporto,
senza sborsare un nichelino a compenso di sette mesi di lavoro,
rubandomelo. Senza le sue escandescenze starei ancora sprecando
neuroni scrivendo sciocche applicazioni Delphi su Window$. Ho *dovuto*
prendermi questa vacanza, per tornare ad estasiarmi nell'arte della
programmazione.

Più seriamente, dedico questo lavoro a mio fratello Fausto, che
immagino costruire perfetti tavoli da Carrom e insegnare buoni tiri
nel suo `निर्वाण`_: non fosse stato per lui, né Scarry né SoL avrebbero
visto la luce.

.. _निर्वाण: http://it.wikipedia.org/wiki/Nirvana_(religione)

Riconoscimenti
==============

Voglio dire "Grazie! alle varie comunità che hanno reso più facile
e stimolante l'implementazione di questo progetto:

Python_
  Le cose semplici sono facili, quelle difficili possibili. Divertendosi!

SQLAlchemy_
  Utilizzo di database relazionali senza compromessi con una ventata
  di aria fresca.

Pyramid_
  Sviluppare applicazioni web tende ad essere un lavoro
  noioso… senza un framework semplice e versatile.

ExtJS_
  Rimango ancora stupito vedendo quanto possa essere efficace una
  interfaccia grafica dentro un (buon) browser web.

SCR_
  I buoni amici sono una cosa preziosa!

.. _python: http://www.python.it/
.. _sqlalchemy: http://www.sqlalchemy.org/
.. _pyramid: http://www.pylonsproject.org/
.. _extjs: http://www.sencha.com/products/extjs/
.. _scr: https://www.facebook.com/Scarambol
