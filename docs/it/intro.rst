.. -*- coding: utf-8 -*-
.. :Project:   SoL -- Introduction
.. :Created:   sab 8 feb 2020, 13:54:16
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2020 Lele Gaifax
..

==============
 Introduzione
==============

SoL è un'applicazione web che consente di gestire tornei di Carrom, organizzati in campionati:
negli anni è diventata alquanto articolata e flessibile, per poter far fronte ad un ampio
spettro di situazioni, dai semplici tornei amatoriali fino a competizioni internazionali.


Concetti
========

Utente
  Una persona che ha un account nel sistema, in grado di gestire *giocatori*, *club*,
  *campionati* e *tornei*.

  La maggior parte degli utenti possono creare i loro *club*, *campionati* e così via, ad
  eccezione dei *giocatori*.

  L'*amministratore* può concedere due permessi addizionali ad un utente:

  a. il permesso di :ref:`gestire chi è responsabile delle entità <gestore responsabili>`

     Questo permette di modificare il *responsabile* delle entità, anche di quelle non gestite
     dall'utente stesso.

  b. il permesso di :ref:`gestire i giocatori <gestore giocatori>`

     Questo consente l'inserimento di nuovi *giocatori*, senza il quale un utente può iscrivere
     al suo nuovo torneo solo giocatori `già presenti` nel database.

Amministratore
  Uno speciale *utente* con super poteri, che può modificare qualsiasi cosa nel sistema.

Responsabile
  Il particolare *utente* che `possiede` una entità (cioè, *giocatori*, *club*, *tornei* e così
  via), generalmente chi le ha create inizialmente. Mentre chiunque può vedere praticamente
  ogni cosa, le entità possono essere modificate esclusivamente dall'utente che ne è
  responsabile (o dall'*amministratore*).

Giocatore
  Una persona che gioca a Carrom e che può partecipare ai *tornei*.

  Sono generalmente associati ad un particolare *club* o a una *federazione*.

Concorrente
  Uno (nei tornei di *singolo) o più (nei tornei di *doppio* o a *squadre*) *giocatori*
  iscritti a giocare in un *torneo*

Club o Federazione
  Una associazione di *giocatori*, che può organizzare dei *campionati*.

Torneo
  Una competizione di Carrom, che avviene in qualche posto in un certo giorno.

  Sono collegati a un *campionato* e sono quindi gestiti da un particolare *club*, sebbene
  possano essere ospitati da un club diverso.

Turno
  Un singolo turno di un torneo: a seconda del numero di *concorrenti*, un torneo può essere
  composto da diversi turni, solitamente sette.

  Ogni turno comporta uno o più *incontri* tra due *concorrenti*.

Incontro
  Due *concorrenti* che giocano uno contro l'altro una o più *partite*, finché uno dei due
  raggiunge i 25 punti oppure si esaurisce il tempo a disposizione.

Campionati
  Un certo numero di *tornei*, organizzati da un certo *club*, che condividono alcune
  impostazioni in particolare il tipo di *premiazione*, in modo da poter calcolare una
  `classifica del campionato`.

  Generalmente un campionato è `stagionale`, e quindi un certo club può averne molti,
  tipicamente uno `correntemente attivo`, gli altri `storici`. Un nuovo torneo può essere
  creato solo in un campionato attivo.

Valutazione
  È un modo per calcolare la `forza` di un *giocatore* con una `formula statistica`__, usato
  principalmente per determinare gli accoppiamenti i *turni* iniziali di un *torneo*. Viene
  automaticamente ricalcolata al termine di ogni torneo, considerando tutti gli *incontri*
  disputati.

__ https://en.wikipedia.org/wiki/Glicko_rating_system


Configurazione minima
=====================

La prima cosa di cui hai bisogno è un `account` sul sistema, cioè registrarti come
*utente* in modo da poterti :ref:`autenticare <autenticazione>` e quindi operare.

Per farlo, puoi contattare l'`amministratore` del sistema o utilizzare il :ref:`procedimento di
registrazione <auto-registrazione>` automatico.

Non appena ottenuta l'autorizzazione, questi sono i passi necessari per gestire il tuo prossimo
torneo:

.. contents::
   :local:

1. Scegli o creati un nuovo club
--------------------------------

Il primo passo nella creazione di un torneo è quello di aprire la finestra :ref:`gestione dei
club <gestione club>`, o dal menu popup principale situato nell'angolo in basso a sinistra
dell'applicazione, o direttamente dall'icona ``I miei club`` nell'angolo in alto a sinistra.
Quest'ultima elencherà solo i club che puoi gestire, di solito quelli di tua proprietà.

Qui devi selezionare il club che gestirà l'evento.

Se il club non è già presente nel database puoi crearne uno nuovo cliccando su ``Aggiungi`` e
compilando le informazioni necessarie: troverai in particolare due campi importanti:
``Abbinamenti`` e ``Premi`` che saranno utili in seguito. Per favore, compilali: se il tuo club
organizza normalmente piccoli tornei locali con meno di 15 giocatori, è meglio scegliere
``Classifica`` e ``Premi fissi``, mentre con più giocatori potrebbero essere più interessanti
le altre opzioni.

Alcune delle impostazioni inserite qui verranno usate come valori predefiniti quando si creerà
un nuovo campionato.

2. Scegli o creati un nuovo campionato
--------------------------------------

Una volta scelto il vostro club cliccate su ``Campionati`` nel menu della finestra, che aprirà
la :ref:`lista dei campionati <gestione campionati>` gestiti da quel club.

Qui puoi selezionare il campionato corretto di cui fa parte il tuo evento o creare un nuovo
campionato cliccando su ``Aggiungi``.

Quando si aggiunge un nuovo campionato devono essere compilati diversi campi importanti:

Giocatori
  Qui devi scegliere se i tornei del campionato saranno `Singoli` o `Doppi`: 1 giocatore per i
  singoli, 2 giocatori per i doppi.

Scarta
  Questo campo è necessario solo se si vogliono ignorare i risultati peggiori in una serie di
  tornei durante l'anno ad esempio ci sono 8 eventi e solo i migliori 6 risultati saranno presi
  per la classifica finale.

Valutazione
  Il rating è davvero importante solo per stabilire gli accoppiamenti dei primi turni, ed è
  utile soprattutto per i grandi eventi. Si basa su semplici statistiche: una persona con una
  valutazione più alta `probabilmente` vincerà una partita contro una persona con un rating più
  basso. Non è basato sulla classifica del torneo, ma solo sulle singole partite giocate.

  Ci sono quattro livelli di valutazione che possono essere associati a un torneo:

  Livello 1
    tornei internazionali (Eurocup, ICF cup, International Open)

  Livello 2
    tornei nazionali

  Livello 3
    tornei regionali

  Livello 4
    tornei locali o amatoriali

  Se associ una valutazione al tuo campionato, verrà usata come scelta di default quando
  creerai nuovi tornei.

  .. important:: **Per favore** scegli o crea la valutazione al livello più basso/ragionevole
                 possibile.

  .. hint:: Se devi creare una nuova valutazione, *per favore* dagli un nome comprensibile e
            possibilmente associala al tuo club, così da risultare facilmente riconoscibile.

Abbinamenti
  Quello che selezioni verrà usato come scelta di default alla creazione di un nuovo torneo.

Premi
  Questo determina il tipo di premiazione che verrà usata al termine dei tornei associati al
  campionato.

3. Crea un nuovo torneo
-----------------------

Quando hai selezionato il giusto campionato clicca su ``Tornei`` nel menu della finestra, che
aprirà la `lista dei tornei <gestione tornei>` associati a quel campionato.

A questo punto sei pronto per creare un nuovo torneo: clicca su ``Aggiungi`` e inserisci le
informazioni richieste, alcune delle quali saranno già pre-impostate con quelle definite sul
club e sul campionato.

.. note:: Se imposti una data futura l'evento non sarà visibile nella lista e per trovarlo
          dovrai azzerare il filtro sulla data usando il pulsante di ricerca.

Scegli se dovrà esserci una finale e di che tipo, la durata degli incontri e il tempo di
preavviso che verranno usati nel conto alla rovescia.

`Ritarda abbinamenti teste di serie` è utile principalmente nei tornei con pochi giocatori e
viene usato il criterio di abbinamento ``classifica``, quando non desideri abbinare i più forti
già nei primi turni.

`Ritarda abbinamenti dei connazionali` consente di evitare abbinamenti di concorrenti della
stessa nazione già nei primi turni, utile primariamente nei tornei internazionali.

`Ritiri` consente di scegliere un aggiustamento nel calcolo dei bucholz qualora uno dei
concorrenti sia costretto ad abbandonare il torneo (vedi :ref:`dettagli <ritiri>`).

Quando hai fatto, clicca su ``Dettagli`` che aprira la :ref:`finestra di gestione del torneo
<gestione torneo>`, e inizia con l'iscrivere i partecipanti scegliendoli dal database: clicca
su ``Mostra tutti i giocatori`` nel caso di un nuovo torneo. Usa la funzione di ricerca per
trovare i giocatori per nome, nazionalità ecc.

Per poter aggiungere nuovi giocatori devi avere il permesso di :ref:`gestione giocatori
<gestore giocatori>`, oppure dovrai chiedere al manager nazionale di aggiungerli prima di
creare il torneo.

Ora sei pronto per generare il primo turno e cominciare a giocare!

.. rubric:: Note

.. important:: Ricordati di cliccare su ``Salva`` dopo ogni modifica apportata.

.. hint:: Puoi stampare i cartellini di gioco con i nomi dei giocatori oppure in bianco; puoi
          stampare le tessere di riconoscimento per ciascun giocatore così come quelle complete
          dei loro incontri al termine del torneo. È possibile ottenere la classifica juniores
          oppure quella per nazioni oppure quella femminile.

.. hint:: In ogni finestra, l'icona ``?`` in alto a destra nel suo bordo ti porta nella sezione
          relativa del manuale utente.
