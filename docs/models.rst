.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   sab 08 nov 2008 20:45:42 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2008, 2009, 2010, 2013, 2014 Lele Gaifax
..

==============================================
 :mod:`sol.models` -- SQLAlchemy modelization
==============================================

.. automodule:: sol.models

.. autoclass:: sol.models.AbstractBase
   :members:

.. autoclass:: sol.models.GloballyUnique
   :members:


.. toctree::
   :maxdepth: 2

   bio
   entities
   utils
