# nixos-20.09-small Released on 2021-04-17
NIXPKGS_REV="c7e905b6a971dad2c26a31960053176c770a812a";
export NIX_PATH="nixpkgs=https://github.com/NixOs/nixpkgs/archive/${NIXPKGS_REV}.tar.gz"
