championships: []
clubs:
- {couplings: serial, description: Polish Carrom Association, guid: a68029446af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: POL, prizes: fixed, siteurl: www.carrom.pl}
- {couplings: serial, description: Villa Loto Carrom Club, emblem: ccvl.png, guid: a67d205a6af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: ITA, prizes: fixed}
- {couplings: serial, description: Carrom Club Verona, guid: a67ec5046af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: ITA, prizes: fixed}
- {couplings: dazed, description: Federazione Italiana Carrom, emblem: fic.png, guid: a67d9b0c6af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: ITA, prizes: fixed, siteurl: 'http://www.carromitaly.com/'}
- {couplings: serial, description: Italian Carrom Federation, guid: a680dc366af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: ITA, prizes: fixed, siteurl: www.carromitaly.com}
- {couplings: serial, description: Carrom Club Milano, emblem: ccm.png, guid: a67d5c0a6af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: ITA, prizes: fixed, siteurl: 'http://www.carromclubmilano.it/'}
- {couplings: serial, description: EuroCup, guid: a67eff386af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', prizes: fixed}
- {couplings: serial, description: United Kingdom Carrom Federation, guid: a67f40746af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: GBR, prizes: fixed}
- {couplings: serial, description: French Carrom Federation, guid: a67f7b026af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: FRA, prizes: fixed, siteurl: www.carrom.net}
- {couplings: serial, description: German Carrom Federation, guid: a67fb55e6af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: DEU, prizes: fixed, siteurl: www.carrom.de}
- {couplings: serial, description: Slovenia, guid: a67fef246af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: SVN, prizes: fixed}
- {couplings: serial, description: Spanish Carrom Association, guid: a68171006af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: ESP, prizes: fixed}
- {couplings: serial, description: Czech Carrom Association, guid: a6809fb46af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: CZE, prizes: fixed, siteurl: www.carrom.cz}
- {couplings: serial, description: Swedish Carrom Federation, guid: a681207e6af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: SWE, prizes: fixed}
players:
- {citizenship: true, club: 2, firstname: Fabio, guid: a68e5eba6af811e3bee53085a99ccac7,
  lastname: Tomasi, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  portrait: ftomasi.png, sex: M}
- {citizenship: true, club: 3, firstname: Daniele, guid: a77bd87a6af811e3bee53085a99ccac7,
  lastname: Da Fatti, modified: !!timestamp '2013-12-22 11:03:14', nationality: ITA,
  nickname: daniele, sex: M}
- {citizenship: true, club: 4, firstname: Rodolfo, guid: a692707c6af811e3bee53085a99ccac7,
  lastname: Donnini, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  portrait: rodolfo.jpg, sex: M}
- {citizenship: true, club: 5, firstname: Lucia Elene, guid: a8a543946af811e3bee53085a99ccac7,
  lastname: Ammazzalamorte, modified: !!timestamp '2013-12-22 11:03:16', nationality: ITA,
  sex: F}
- {citizenship: true, club: 6, firstname: Paolo, guid: a697a4de6af811e3bee53085a99ccac7,
  lastname: Ometto, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  portrait: paolo.jpg, sex: M}
- {citizenship: true, club: 6, firstname: Stefano, guid: a69f74e86af811e3bee53085a99ccac7,
  lastname: Fabiano, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  nickname: stefano, sex: M}
- {citizenship: true, club: 4, firstname: Giorgio, guid: a71151bc6af811e3bee53085a99ccac7,
  lastname: Balicchi, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  sex: M}
- {citizenship: true, club: 4, firstname: Elisa, guid: a6b21d326af811e3bee53085a99ccac7,
  lastname: Zucchiatti, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  nickname: elisa, portrait: elisa.jpg, sex: F}
- {citizenship: true, club: 4, firstname: Francesco, guid: a718d4506af811e3bee53085a99ccac7,
  lastname: Pedicini, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  sex: M}
- {citizenship: true, club: 5, firstname: Amitha, guid: a89c7b6a6af811e3bee53085a99ccac7,
  lastname: Ubhayatunga, modified: !!timestamp '2013-12-22 11:03:16', nationality: ITA,
  sex: M}
- {citizenship: true, club: 7, firstname: Armin, guid: a7a124fe6af811e3bee53085a99ccac7,
  lastname: Steiner, modified: !!timestamp '2013-12-22 11:03:14', nationality: DEU,
  sex: M}
- {citizenship: true, club: 7, firstname: Hans, guid: a7a091606af811e3bee53085a99ccac7,
  lastname: Faller, modified: !!timestamp '2013-12-22 11:03:14', nationality: DEU,
  sex: M}
- {citizenship: true, club: 7, firstname: Frank, guid: a7a1b6d06af811e3bee53085a99ccac7,
  lastname: Fehr, modified: !!timestamp '2013-12-22 11:03:14', nationality: DEU, sex: M}
- {citizenship: true, club: 7, firstname: Jan, guid: a7a41e166af811e3bee53085a99ccac7,
  lastname: Geister, modified: !!timestamp '2013-12-22 11:03:14', nationality: DEU,
  sex: M}
- {citizenship: true, club: 8, firstname: Abdul, guid: a7a8ccb86af811e3bee53085a99ccac7,
  lastname: Khan, modified: !!timestamp '2013-12-22 11:03:14', nationality: GBR, sex: M}
- {citizenship: true, club: 7, firstname: Shaheen, guid: a7cfda066af811e3bee53085a99ccac7,
  lastname: Miah, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR, sex: M}
- {citizenship: true, club: 7, firstname: Bernhard, guid: a7b2c9d46af811e3bee53085a99ccac7,
  lastname: Sander, modified: !!timestamp '2013-12-22 11:03:14', nationality: DEU,
  sex: M}
- {citizenship: true, club: 7, firstname: Susanne, guid: a849852c6af811e3bee53085a99ccac7,
  lastname: Schackert, modified: !!timestamp '2013-12-22 11:03:15', nationality: DEU,
  sex: F}
- {citizenship: true, club: 7, firstname: Fabian, guid: a7b7fa446af811e3bee53085a99ccac7,
  lastname: Pereira, modified: !!timestamp '2013-12-22 11:03:14', nationality: FRA,
  sex: M}
- {citizenship: true, club: 9, firstname: Sieger, guid: a8990cfa6af811e3bee53085a99ccac7,
  lastname: Kevin, modified: !!timestamp '2013-12-22 11:03:16', nationality: FRA,
  sex: M}
- {citizenship: true, club: 10, firstname: Gunter, guid: a7b92d886af811e3bee53085a99ccac7,
  lastname: Seibert, modified: !!timestamp '2013-12-22 11:03:14', nationality: DEU,
  sex: M}
- {citizenship: true, club: 10, firstname: Paul, guid: a8aa8a3e6af811e3bee53085a99ccac7,
  lastname: Schneider, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU,
  sex: M}
- {citizenship: true, club: 7, firstname: Steffen, guid: a7bb8df86af811e3bee53085a99ccac7,
  lastname: Eder, modified: !!timestamp '2013-12-22 11:03:14', nationality: DEU, sex: M}
- {citizenship: true, club: 7, firstname: Nick, guid: a801969a6af811e3bee53085a99ccac7,
  lastname: Humbert, modified: !!timestamp '2013-12-22 11:03:15', nationality: DEU,
  sex: M}
- {citizenship: true, club: 8, firstname: Mukith, guid: a7c4dbd86af811e3bee53085a99ccac7,
  lastname: Hussain, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR,
  sex: M}
- {citizenship: true, club: 8, firstname: Azir, guid: a8086d1c6af811e3bee53085a99ccac7,
  lastname: Uddin, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR,
  sex: M}
- {citizenship: true, club: 11, firstname: Thangaraj, guid: a7ca16d46af811e3bee53085a99ccac7,
  lastname: Kuppusamy, modified: !!timestamp '2013-12-22 11:03:15', nationality: SVN,
  sex: M}
- {citizenship: true, club: 6, firstname: Vittorio, guid: a69ab1a66af811e3bee53085a99ccac7,
  lastname: Canisi, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  portrait: vittorio.jpg, sex: M}
- {citizenship: true, club: 8, firstname: Abdul, guid: a7d780d06af811e3bee53085a99ccac7,
  lastname: Malik, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR,
  sex: M}
- {citizenship: true, club: 8, firstname: Sunahar, guid: a7cf45aa6af811e3bee53085a99ccac7,
  lastname: Ali, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR, sex: M}
- {citizenship: true, club: 10, firstname: Wolfgang, guid: a7d8a0a06af811e3bee53085a99ccac7,
  lastname: Peter, modified: !!timestamp '2013-12-22 11:03:15', nationality: DEU,
  sex: M}
- {citizenship: true, club: 10, firstname: Mark, guid: a8bc997c6af811e3bee53085a99ccac7,
  lastname: Muller -Linow, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU,
  sex: M}
- {citizenship: true, club: 8, firstname: Ahad, guid: a80afb866af811e3bee53085a99ccac7,
  lastname: Miah, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR, sex: M}
- {citizenship: true, club: 8, firstname: Abdus, guid: a8a125486af811e3bee53085a99ccac7,
  lastname: Shahid, modified: !!timestamp '2013-12-22 11:03:16', nationality: GBR,
  sex: M}
- {citizenship: true, club: 7, firstname: Julian, guid: a80d3d106af811e3bee53085a99ccac7,
  lastname: Perkin, modified: !!timestamp '2013-12-22 11:03:15', nationality: FRA,
  sex: M}
- {citizenship: true, club: 10, firstname: Andreas, guid: a8bd25186af811e3bee53085a99ccac7,
  lastname: Fischer, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU,
  sex: M}
- {citizenship: true, club: 7, firstname: Giulio, guid: a82bacc86af811e3bee53085a99ccac7,
  lastname: Martinelli, modified: !!timestamp '2013-12-22 11:03:15', nationality: ITA,
  sex: M}
- {citizenship: true, club: 7, firstname: Andrea, guid: a7ce242c6af811e3bee53085a99ccac7,
  lastname: Martinelli, modified: !!timestamp '2013-12-22 11:03:15', nationality: ITA,
  sex: M}
- {citizenship: true, club: 7, firstname: Mervin, guid: a7c97abc6af811e3bee53085a99ccac7,
  lastname: Kalinga, modified: !!timestamp '2013-12-22 11:03:15', nationality: CHE,
  sex: M}
- {citizenship: true, club: 7, firstname: Carlos, guid: a7c262726af811e3bee53085a99ccac7,
  lastname: Fernandez, modified: !!timestamp '2013-12-22 11:03:15', nationality: CHE,
  sex: M}
- {citizenship: true, club: 7, firstname: Balendran, guid: a844b6fa6af811e3bee53085a99ccac7,
  lastname: Kanagan, modified: !!timestamp '2013-12-22 11:03:15', nationality: DEU,
  sex: M}
- {citizenship: true, club: 10, firstname: Soundararajah, guid: a8b884866af811e3bee53085a99ccac7,
  lastname: Rangasamy, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU,
  sex: M}
- {citizenship: true, club: 7, firstname: Uwe, guid: a8400ee86af811e3bee53085a99ccac7,
  lastname: Schneider, modified: !!timestamp '2013-12-22 11:03:15', nationality: DEU,
  sex: M}
- {citizenship: true, club: 7, firstname: Armin, guid: a7b8999a6af811e3bee53085a99ccac7,
  lastname: Schneider, modified: !!timestamp '2013-12-22 11:03:14', nationality: DEU,
  sex: M}
- {citizenship: true, club: 7, firstname: Mehedi, guid: a855bbda6af811e3bee53085a99ccac7,
  lastname: Hassan, modified: !!timestamp '2013-12-22 11:03:15', nationality: SWE,
  sex: M}
- {citizenship: true, club: 7, firstname: Saravanan, guid: a85354126af811e3bee53085a99ccac7,
  lastname: Babu, modified: !!timestamp '2013-12-22 11:03:15', nationality: SWE, sex: M}
- {citizenship: true, club: 7, firstname: Amar, guid: a8577ccc6af811e3bee53085a99ccac7,
  lastname: Sanakal, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR,
  sex: M}
- {citizenship: true, club: 8, firstname: Anhar, guid: a7ae92f66af811e3bee53085a99ccac7,
  lastname: Ali, modified: !!timestamp '2013-12-22 11:03:14', nationality: GBR, sex: M}
- {citizenship: true, club: 7, firstname: Igor, guid: a85af91a6af811e3bee53085a99ccac7,
  lastname: Khattry, modified: !!timestamp '2013-12-22 11:03:16', nationality: ESP,
  sex: M}
- {citizenship: true, club: 12, firstname: Luis Felipe, guid: a8b4721a6af811e3bee53085a99ccac7,
  lastname: Mendez Banderas, modified: !!timestamp '2013-12-22 11:03:16', nationality: ESP,
  sex: M}
- {citizenship: true, club: 7, firstname: Barbara, guid: a85f15546af811e3bee53085a99ccac7,
  lastname: Thomas, modified: !!timestamp '2013-12-22 11:03:16', nationality: CHE,
  sex: F}
- {citizenship: true, club: 7, firstname: Kurt, guid: a85cc3266af811e3bee53085a99ccac7,
  lastname: Scherrer, modified: !!timestamp '2013-12-22 11:03:16', nationality: CHE,
  sex: M}
- {citizenship: true, club: 13, firstname: Cyrus, guid: a89bdfa26af811e3bee53085a99ccac7,
  lastname: Skaria, modified: !!timestamp '2013-12-22 11:03:16', nationality: CZE,
  sex: M}
- {citizenship: true, club: 13, firstname: Honza, guid: a8a7995a6af811e3bee53085a99ccac7,
  lastname: Tesitel, modified: !!timestamp '2013-12-22 11:03:16', nationality: CZE,
  sex: M}
- {citizenship: true, club: 14, firstname: Gunnar, guid: a89e433c6af811e3bee53085a99ccac7,
  lastname: Von Arnold, modified: !!timestamp '2013-12-22 11:03:16', nationality: SWE,
  sex: M}
- {citizenship: true, club: 14, firstname: Olof, guid: a8af19a06af811e3bee53085a99ccac7,
  lastname: Klint, modified: !!timestamp '2013-12-22 11:03:16', nationality: SWE,
  sex: M}
- {citizenship: true, club: 9, firstname: Thomas, guid: a8a2f2b06af811e3bee53085a99ccac7,
  lastname: Chobe, modified: !!timestamp '2013-12-22 11:03:16', nationality: FRA,
  sex: M}
- {citizenship: true, club: 7, firstname: Thierry, guid: a8114cf26af811e3bee53085a99ccac7,
  lastname: Huchet, modified: !!timestamp '2013-12-22 11:03:15', nationality: FRA,
  sex: M}
- {citizenship: true, club: 10, firstname: Abdullah, guid: a8a5d1566af811e3bee53085a99ccac7,
  lastname: Nauandish, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU,
  sex: M}
- {citizenship: true, club: 7, firstname: Andreas, guid: a7a5c20c6af811e3bee53085a99ccac7,
  lastname: Hurtig, modified: !!timestamp '2013-12-22 11:03:14', nationality: DEU,
  sex: M}
- {citizenship: true, club: 9, firstname: Prabagarane, guid: a8ab21a66af811e3bee53085a99ccac7,
  lastname: Deiva, modified: !!timestamp '2013-12-22 11:03:16', nationality: FRA,
  sex: M}
- {citizenship: true, club: 7, firstname: Yoann, guid: a810239a6af811e3bee53085a99ccac7,
  lastname: Pidial, modified: !!timestamp '2013-12-22 11:03:15', nationality: FRA,
  sex: M}
- {citizenship: true, club: 1, firstname: Jakub, guid: a8abb92c6af811e3bee53085a99ccac7,
  lastname: Sasinski, modified: !!timestamp '2013-12-22 11:03:16', nationality: POL,
  sex: M}
- {citizenship: true, club: 1, firstname: Bartosz, guid: a8b3dca66af811e3bee53085a99ccac7,
  lastname: Sasinski, modified: !!timestamp '2013-12-22 11:03:16', nationality: POL,
  sex: M}
- {citizenship: true, club: 13, firstname: Anna, guid: a8ae8e4a6af811e3bee53085a99ccac7,
  lastname: Mazur Skaria, modified: !!timestamp '2013-12-22 11:03:16', nationality: CZE,
  sex: F}
- {citizenship: true, club: 13, firstname: Pavel, guid: a7ef5e3a6af811e3bee53085a99ccac7,
  lastname: Schierl, modified: !!timestamp '2013-12-22 11:03:15', nationality: CZE,
  sex: M}
- {citizenship: true, club: 8, firstname: Chandan, guid: a8afbbe46af811e3bee53085a99ccac7,
  lastname: Narkar, modified: !!timestamp '2013-12-22 11:03:16', nationality: GBR,
  sex: M}
- {citizenship: true, club: 8, firstname: Ish, guid: a80c19da6af811e3bee53085a99ccac7,
  lastname: Kumar, modified: !!timestamp '2013-12-22 11:03:15', nationality: GBR,
  sex: M}
- {citizenship: true, club: 10, firstname: Eshan, guid: a8b0e67c6af811e3bee53085a99ccac7,
  lastname: Mannan, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU,
  sex: M}
- {citizenship: true, club: 7, firstname: Kanagan, guid: a85fa47e6af811e3bee53085a99ccac7,
  lastname: Santhakumar, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU,
  sex: M}
- {citizenship: true, club: 9, firstname: Thierry, guid: a8b514546af811e3bee53085a99ccac7,
  lastname: Berthelemy, modified: !!timestamp '2013-12-22 11:03:16', nationality: FRA,
  sex: M}
- {citizenship: true, club: 7, firstname: Sattirabady, guid: a81a32f46af811e3bee53085a99ccac7,
  lastname: Jupiter, modified: !!timestamp '2013-12-22 11:03:15', nationality: FRA,
  sex: M}
- {citizenship: true, club: 8, firstname: Misbah, guid: a8b5a98c6af811e3bee53085a99ccac7,
  lastname: Ahmed, modified: !!timestamp '2013-12-22 11:03:16', nationality: GBR,
  sex: M}
- {citizenship: true, club: 8, firstname: Rahat, guid: a8b34c826af811e3bee53085a99ccac7,
  lastname: Islam, modified: !!timestamp '2013-12-22 11:03:16', nationality: GBR,
  sex: M}
- {citizenship: true, club: 1, firstname: Wojtek, guid: a8b7e9e06af811e3bee53085a99ccac7,
  lastname: Rzewuski, modified: !!timestamp '2013-12-22 11:03:16', nationality: POL,
  sex: M}
- {citizenship: true, club: 1, firstname: Nagaraj, guid: a89d11ba6af811e3bee53085a99ccac7,
  lastname: Ramakrishna, modified: !!timestamp '2013-12-22 11:03:16', nationality: POL,
  sex: M}
- {citizenship: true, club: 10, firstname: Clement, guid: a8b918ec6af811e3bee53085a99ccac7,
  lastname: Anthony, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU,
  sex: M}
- {citizenship: true, club: 10, firstname: Daniel, guid: a89da67a6af811e3bee53085a99ccac7,
  lastname: Monoharan, modified: !!timestamp '2013-12-22 11:03:16', nationality: DEU,
  sex: M}
- {citizenship: true, club: 8, firstname: Harvery, guid: a8b9ae4c6af811e3bee53085a99ccac7,
  lastname: Gomes, modified: !!timestamp '2013-12-22 11:03:16', nationality: GBR,
  sex: M}
- {citizenship: true, club: 8, firstname: Joseph, guid: a8a705e46af811e3bee53085a99ccac7,
  lastname: Gloosberg, modified: !!timestamp '2013-12-22 11:03:16', nationality: GBR,
  sex: M}
- {citizenship: true, club: 1, firstname: Rafal, guid: a8ba4fb46af811e3bee53085a99ccac7,
  lastname: Szczepanik, modified: !!timestamp '2013-12-22 11:03:16', nationality: POL,
  sex: M}
- {citizenship: true, club: 7, firstname: Nitesh, guid: a7c6900e6af811e3bee53085a99ccac7,
  lastname: Sinha, modified: !!timestamp '2013-12-22 11:03:15', nationality: POL,
  nickname: Nick, sex: M}
rates: []
ratings: []
seasons:
- {closed: false, club: 1, couplings: serial, description: Double Event, guid: a931fa786af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:17', playersperteam: 2, prizefactor: '0',
  prizes: millesimal, skipworstprizes: 0}
---
competitors:
- bucholz: 50
  netscore: -13
  players: [1, 2]
  points: 8
  prize: '616'
  totscore: 97
- bucholz: 39
  netscore: -2
  players: [3, 4]
  points: 4
  prize: '136'
  totscore: 101
- bucholz: 38
  netscore: 10
  players: [5, 6]
  points: 6
  prize: '280'
  totscore: 78
- bucholz: 52
  netscore: -18
  players: [7, 8]
  points: 8
  prize: '664'
  totscore: 100
- bucholz: 53
  netscore: 48
  players: [9, 10]
  points: 8
  prize: '736'
  totscore: 126
- bucholz: 52
  netscore: -9
  players: [11, 12]
  points: 8
  prize: '688'
  totscore: 105
- bucholz: 56
  netscore: 58
  players: [13, 14]
  points: 10
  prize: '832'
  totscore: 135
- bucholz: 65
  netscore: 73
  players: [15, 16]
  points: 10
  prize: '904'
  totscore: 144
- bucholz: 49
  netscore: -8
  players: [17, 18]
  points: 7
  prize: '496'
  totscore: 107
- bucholz: 69
  netscore: 59
  players: [19, 20]
  points: 10
  prize: '952'
  totscore: 136
- bucholz: 48
  netscore: -10
  players: [21, 22]
  points: 5
  prize: '232'
  totscore: 86
- bucholz: 60
  netscore: -41
  players: [23, 24]
  points: 7
  prize: '544'
  totscore: 84
- bucholz: 39
  netscore: 10
  players: [25, 26]
  points: 6
  prize: '304'
  totscore: 107
- bucholz: 51
  netscore: 18
  players: [27, 28]
  points: 8
  prize: '640'
  totscore: 111
- bucholz: 60
  netscore: 95
  players: [29, 30]
  points: 14
  prize: '1000'
  totscore: 158
- bucholz: 65
  netscore: 71
  players: [31, 32]
  points: 10
  prize: '880'
  totscore: 147
- bucholz: 53
  netscore: -6
  players: [33, 34]
  points: 7
  prize: '520'
  totscore: 95
- bucholz: 29
  netscore: -74
  players: [35, 36]
  points: 3
  prize: '40'
  totscore: 37
- bucholz: 50
  netscore: -10
  players: [37, 38]
  points: 6
  prize: '400'
  totscore: 110
- bucholz: 49
  netscore: 34
  players: [39, 40]
  points: 8
  prize: '592'
  totscore: 121
- bucholz: 64
  netscore: 28
  players: [41, 42]
  points: 10
  prize: '856'
  totscore: 112
- bucholz: 46
  netscore: 38
  players: [43, 44]
  points: 10
  prize: '808'
  totscore: 123
- bucholz: 55
  netscore: 96
  players: [45, 46]
  points: 12
  prize: '976'
  totscore: 155
- bucholz: 60
  netscore: -20
  players: [47, 48]
  points: 8
  prize: '760'
  totscore: 95
- bucholz: 38
  netscore: -74
  players: [49, 50]
  points: 3
  prize: '64'
  totscore: 57
- bucholz: 35
  netscore: -14
  players: [51, 52]
  points: 6
  prize: '256'
  totscore: 97
- bucholz: 47
  netscore: 25
  players: [53, 54]
  points: 6
  prize: '352'
  totscore: 116
- bucholz: 38
  netscore: -7
  players: [55, 56]
  points: 4
  prize: '112'
  totscore: 73
- bucholz: 51
  netscore: 10
  players: [57, 58]
  points: 9
  prize: '784'
  totscore: 106
- bucholz: 57
  netscore: -14
  players: [59, 60]
  points: 6
  prize: '448'
  totscore: 95
- bucholz: 44
  netscore: -31
  players: [61, 62]
  points: 6
  prize: '328'
  totscore: 78
- bucholz: 49
  netscore: -29
  players: [63, 64]
  points: 4
  prize: '208'
  totscore: 97
- bucholz: 31
  netscore: -60
  players: [65, 66]
  points: 4
  prize: '88'
  totscore: 51
- bucholz: 65
  netscore: 93
  players: [67, 68]
  points: 10
  prize: '928'
  totscore: 150
- bucholz: 48
  netscore: -20
  players: [69, 70]
  points: 6
  prize: '376'
  totscore: 95
- bucholz: 45
  netscore: -99
  players: [71, 72]
  points: 4
  prize: '184'
  totscore: 45
- bucholz: 53
  netscore: 33
  players: [73, 74]
  points: 8
  prize: '712'
  totscore: 123
- bucholz: 38
  netscore: -21
  players: [75, 76]
  points: 7
  prize: '472'
  totscore: 83
- bucholz: 51
  netscore: -2
  players: [77, 78]
  points: 6
  prize: '424'
  totscore: 97
- bucholz: 42
  netscore: -78
  players: [79, 80]
  points: 4
  prize: '160'
  totscore: 47
- bucholz: 40
  netscore: 36
  players: [81, 82]
  points: 8
  prize: '568'
  totscore: 125
couplings: dazed
currentturn: 7
date: 2013-06-19
description: 17th Carrom Euro Cup - Double
duration: 55
guid: b495d1c86af811e3bee53085a99ccac7
location: Leszno
matches:
- {board: 1, competitor1: 38, competitor2: 34, score1: 1, score2: 25, turn: 1}
- {board: 2, competitor1: 23, competitor2: 37, score1: 20, score2: 15, turn: 1}
- {board: 3, competitor1: 32, competitor2: 26, score1: 25, score2: 4, turn: 1}
- {board: 4, competitor1: 7, competitor2: 10, score1: 6, score2: 25, turn: 1}
- {board: 5, competitor1: 33, competitor2: 19, score1: 0, score2: 25, turn: 1}
- {board: 6, competitor1: 25, competitor2: 18, score1: 12, score2: 12, turn: 1}
- {board: 7, competitor1: 16, competitor2: 3, score1: 25, score2: 0, turn: 1}
- {board: 8, competitor1: 27, competitor2: 30, score1: 8, score2: 18, turn: 1}
- {board: 9, competitor1: 41, competitor2: 12, score1: 17, score2: 21, turn: 1}
- {board: 10, competitor1: 21, competitor2: 39, score1: 17, score2: 4, turn: 1}
- {board: 11, competitor1: 20, competitor2: 17, score1: 18, score2: 21, turn: 1}
- {board: 12, competitor1: 1, competitor2: 40, score1: 18, score2: 9, turn: 1}
- {board: 13, competitor1: 5, competitor2: 31, score1: 25, score2: 6, turn: 1}
- {board: 14, competitor1: 14, competitor2: 2, score1: 25, score2: 10, turn: 1}
- {board: 15, competitor1: 11, competitor2: 29, score1: 4, score2: 25, turn: 1}
- {board: 16, competitor1: 22, competitor2: 6, score1: 0, score2: 25, turn: 1}
- {board: 17, competitor1: 4, competitor2: 24, score1: 12, score2: 25, turn: 1}
- {board: 18, competitor1: 28, competitor2: 15, score1: 0, score2: 25, turn: 1}
- {board: 19, competitor1: 8, competitor2: 13, score1: 25, score2: 0, turn: 1}
- {board: 20, competitor1: 35, competitor2: 36, score1: 22, score2: 7, turn: 1}
- {board: 21, competitor1: 9, competitor2: null, score1: 25, score2: 0, turn: 1}
- {board: 1, competitor1: 19, competitor2: 14, score1: 9, score2: 22, turn: 2}
- {board: 2, competitor1: 16, competitor2: 35, score1: 25, score2: 6, turn: 2}
- {board: 3, competitor1: 9, competitor2: 24, score1: 5, score2: 18, turn: 2}
- {board: 4, competitor1: 6, competitor2: 21, score1: 0, score2: 25, turn: 2}
- {board: 5, competitor1: 15, competitor2: 30, score1: 25, score2: 1, turn: 2}
- {board: 6, competitor1: 8, competitor2: 1, score1: 25, score2: 0, turn: 2}
- {board: 7, competitor1: 34, competitor2: 23, score1: 25, score2: 10, turn: 2}
- {board: 8, competitor1: 29, competitor2: 12, score1: 21, score2: 21, turn: 2}
- {board: 9, competitor1: 32, competitor2: 17, score1: 4, score2: 25, turn: 2}
- {board: 10, competitor1: 5, competitor2: 10, score1: 12, score2: 25, turn: 2}
- {board: 11, competitor1: 18, competitor2: 41, score1: 0, score2: 25, turn: 2}
- {board: 12, competitor1: 25, competitor2: 20, score1: 1, score2: 25, turn: 2}
- {board: 13, competitor1: 37, competitor2: 11, score1: 19, score2: 13, turn: 2}
- {board: 14, competitor1: 40, competitor2: 26, score1: 5, score2: 25, turn: 2}
- {board: 15, competitor1: 27, competitor2: 38, score1: 25, score2: 0, turn: 2}
- {board: 16, competitor1: 4, competitor2: 33, score1: 25, score2: 2, turn: 2}
- {board: 17, competitor1: 39, competitor2: 3, score1: 11, score2: 8, turn: 2}
- {board: 18, competitor1: 2, competitor2: 22, score1: 14, score2: 25, turn: 2}
- {board: 19, competitor1: 36, competitor2: 28, score1: 10, score2: 9, turn: 2}
- {board: 20, competitor1: 7, competitor2: 31, score1: 25, score2: 0, turn: 2}
- {board: 21, competitor1: 13, competitor2: null, score1: 25, score2: 0, turn: 2}
- {board: 1, competitor1: 8, competitor2: 15, score1: 17, score2: 20, turn: 3}
- {board: 2, competitor1: 10, competitor2: 34, score1: 13, score2: 12, turn: 3}
- {board: 3, competitor1: 21, competitor2: 16, score1: 3, score2: 25, turn: 3}
- {board: 4, competitor1: 24, competitor2: 17, score1: 21, score2: 19, turn: 3}
- {board: 5, competitor1: 14, competitor2: 12, score1: 10, score2: 15, turn: 3}
- {board: 6, competitor1: 29, competitor2: 32, score1: 21, score2: 7, turn: 3}
- {board: 7, competitor1: 6, competitor2: 7, score1: 5, score2: 25, turn: 3}
- {board: 8, competitor1: 35, competitor2: 13, score1: 21, score2: 15, turn: 3}
- {board: 9, competitor1: 23, competitor2: 39, score1: 25, score2: 4, turn: 3}
- {board: 10, competitor1: 30, competitor2: 1, score1: 11, score2: 18, turn: 3}
- {board: 11, competitor1: 19, competitor2: 26, score1: 25, score2: 9, turn: 3}
- {board: 12, competitor1: 41, competitor2: 37, score1: 6, score2: 25, turn: 3}
- {board: 13, competitor1: 20, competitor2: 27, score1: 22, score2: 14, turn: 3}
- {board: 14, competitor1: 9, competitor2: 22, score1: 15, score2: 13, turn: 3}
- {board: 15, competitor1: 4, competitor2: 5, score1: 5, score2: 23, turn: 3}
- {board: 16, competitor1: 36, competitor2: 25, score1: 19, score2: 14, turn: 3}
- {board: 17, competitor1: 18, competitor2: 2, score1: 0, score2: 25, turn: 3}
- {board: 18, competitor1: 3, competitor2: 31, score1: 14, score2: 8, turn: 3}
- {board: 19, competitor1: 38, competitor2: 11, score1: 13, score2: 13, turn: 3}
- {board: 20, competitor1: 28, competitor2: 40, score1: 4, score2: 12, turn: 3}
- {board: 21, competitor1: 33, competitor2: null, score1: 25, score2: 0, turn: 3}
- {board: 1, competitor1: 16, competitor2: 24, score1: 25, score2: 10, turn: 4}
- {board: 2, competitor1: 12, competitor2: 8, score1: 4, score2: 25, turn: 4}
- {board: 3, competitor1: 10, competitor2: 15, score1: 9, score2: 25, turn: 4}
- {board: 4, competitor1: 35, competitor2: 5, score1: 7, score2: 25, turn: 4}
- {board: 5, competitor1: 29, competitor2: 17, score1: 13, score2: 1, turn: 4}
- {board: 6, competitor1: 34, competitor2: 9, score1: 25, score2: 18, turn: 4}
- {board: 7, competitor1: 14, competitor2: 1, score1: 23, score2: 5, turn: 4}
- {board: 8, competitor1: 21, competitor2: 20, score1: 17, score2: 12, turn: 4}
- {board: 9, competitor1: 19, competitor2: 7, score1: 8, score2: 25, turn: 4}
- {board: 10, competitor1: 23, competitor2: 36, score1: 25, score2: 0, turn: 4}
- {board: 11, competitor1: 37, competitor2: 4, score1: 15, score2: 17, turn: 4}
- {board: 12, competitor1: 30, competitor2: 26, score1: 23, score2: 6, turn: 4}
- {board: 13, competitor1: 32, competitor2: 3, score1: 10, score2: 8, turn: 4}
- {board: 14, competitor1: 41, competitor2: 27, score1: 10, score2: 15, turn: 4}
- {board: 15, competitor1: 6, competitor2: 2, score1: 25, score2: 10, turn: 4}
- {board: 16, competitor1: 39, competitor2: 40, score1: 25, score2: 0, turn: 4}
- {board: 17, competitor1: 13, competitor2: 22, score1: 14, score2: 20, turn: 4}
- {board: 18, competitor1: 33, competitor2: 11, score1: 0, score2: 25, turn: 4}
- {board: 19, competitor1: 31, competitor2: null, score1: 25, score2: 0, turn: 4}
- {board: 20, competitor1: 25, competitor2: 38, score1: 5, score2: 25, turn: 4}
- {board: 21, competitor1: 18, competitor2: 28, score1: 0, score2: 25, turn: 4}
- {board: 1, competitor1: 29, competitor2: 10, score1: 2, score2: 25, turn: 5}
- {board: 2, competitor1: 21, competitor2: 7, score1: 25, score2: 9, turn: 5}
- {board: 3, competitor1: 8, competitor2: 14, score1: 25, score2: 8, turn: 5}
- {board: 4, competitor1: 16, competitor2: 15, score1: 18, score2: 23, turn: 5}
- {board: 5, competitor1: 24, competitor2: 5, score1: 12, score2: 9, turn: 5}
- {board: 6, competitor1: 23, competitor2: 17, score1: 25, score2: 4, turn: 5}
- {board: 7, competitor1: 35, competitor2: 20, score1: 3, score2: 19, turn: 5}
- {board: 8, competitor1: 6, competitor2: 32, score1: 24, score2: 21, turn: 5}
- {board: 9, competitor1: 30, competitor2: 37, score1: 6, score2: 15, turn: 5}
- {board: 10, competitor1: 34, competitor2: 12, score1: 25, score2: 0, turn: 5}
- {board: 11, competitor1: 1, competitor2: 36, score1: 24, score2: 1, turn: 5}
- {board: 12, competitor1: 4, competitor2: 27, score1: 22, score2: 18, turn: 5}
- {board: 13, competitor1: 19, competitor2: 22, score1: 12, score2: 25, turn: 5}
- {board: 14, competitor1: 11, competitor2: 3, score1: 14, score2: 11, turn: 5}
- {board: 15, competitor1: 9, competitor2: 39, score1: 23, score2: 19, turn: 5}
- {board: 16, competitor1: 38, competitor2: 28, score1: 8, score2: 6, turn: 5}
- {board: 17, competitor1: 41, competitor2: 40, score1: 25, score2: 1, turn: 5}
- {board: 18, competitor1: 13, competitor2: 31, score1: 15, score2: 21, turn: 5}
- {board: 19, competitor1: 2, competitor2: 26, score1: 13, score2: 14, turn: 5}
- {board: 20, competitor1: 33, competitor2: 25, score1: 8, score2: 15, turn: 5}
- {board: 21, competitor1: 18, competitor2: null, score1: 25, score2: 0, turn: 5}
- {board: 1, competitor1: 37, competitor2: 1, score1: 16, score2: 23, turn: 6}
- {board: 2, competitor1: 29, competitor2: 5, score1: 15, score2: 13, turn: 6}
- {board: 3, competitor1: 4, competitor2: 6, score1: 19, score2: 10, turn: 6}
- {board: 4, competitor1: 9, competitor2: 20, score1: 6, score2: 25, turn: 6}
- {board: 5, competitor1: 10, competitor2: 8, score1: 14, score2: 20, turn: 6}
- {board: 6, competitor1: 21, competitor2: 24, score1: 20, score2: 9, turn: 6}
- {board: 7, competitor1: 22, competitor2: 12, score1: 15, score2: 5, turn: 6}
- {board: 8, competitor1: 15, competitor2: 34, score1: 15, score2: 13, turn: 6}
- {board: 9, competitor1: 27, competitor2: 31, score1: 12, score2: 13, turn: 6}
- {board: 10, competitor1: 7, competitor2: 14, score1: 20, score2: 5, turn: 6}
- {board: 11, competitor1: 16, competitor2: 23, score1: 4, score2: 25, turn: 6}
- {board: 12, competitor1: 11, competitor2: 17, score1: 5, score2: 10, turn: 6}
- {board: 13, competitor1: 38, competitor2: 30, score1: 15, score2: 20, turn: 6}
- {board: 14, competitor1: 39, competitor2: 36, score1: 25, score2: 8, turn: 6}
- {board: 15, competitor1: 35, competitor2: 41, score1: 11, score2: 20, turn: 6}
- {board: 16, competitor1: 19, competitor2: 32, score1: 23, score2: 20, turn: 6}
- {board: 17, competitor1: 26, competitor2: 25, score1: 25, score2: 4, turn: 6}
- {board: 18, competitor1: 28, competitor2: null, score1: 25, score2: 0, turn: 6}
- {board: 19, competitor1: 18, competitor2: 3, score1: 0, score2: 12, turn: 6}
- {board: 20, competitor1: 13, competitor2: 33, score1: 21, score2: 4, turn: 6}
- {board: 21, competitor1: 2, competitor2: 40, score1: 4, score2: 14, turn: 6}
- {board: 1, competitor1: 17, competitor2: 9, score1: 15, score2: 15, turn: 7}
- {board: 2, competitor1: 24, competitor2: 22, score1: 0, score2: 25, turn: 7}
- {board: 3, competitor1: 7, competitor2: 1, score1: 25, score2: 9, turn: 7}
- {board: 4, competitor1: 8, competitor2: 23, score1: 7, score2: 25, turn: 7}
- {board: 5, competitor1: 34, competitor2: 4, score1: 25, score2: 0, turn: 7}
- {board: 6, competitor1: 10, competitor2: 20, score1: 25, score2: 0, turn: 7}
- {board: 7, competitor1: 15, competitor2: 21, score1: 25, score2: 5, turn: 7}
- {board: 8, competitor1: 5, competitor2: 19, score1: 19, score2: 8, turn: 7}
- {board: 9, competitor1: 29, competitor2: 16, score1: 9, score2: 25, turn: 7}
- {board: 10, competitor1: 37, competitor2: 31, score1: 18, score2: 5, turn: 7}
- {board: 11, competitor1: 14, competitor2: 39, score1: 18, score2: 9, turn: 7}
- {board: 12, competitor1: 38, competitor2: 32, score1: 21, score2: 10, turn: 7}
- {board: 13, competitor1: 6, competitor2: 26, score1: 16, score2: 14, turn: 7}
- {board: 14, competitor1: 35, competitor2: 28, score1: 25, score2: 4, turn: 7}
- {board: 15, competitor1: 36, competitor2: 3, score1: 0, score2: 25, turn: 7}
- {board: 16, competitor1: 30, competitor2: 41, score1: 16, score2: 22, turn: 7}
- {board: 17, competitor1: 12, competitor2: 11, score1: 18, score2: 12, turn: 7}
- {board: 18, competitor1: 27, competitor2: 40, score1: 24, score2: 6, turn: 7}
- {board: 19, competitor1: 13, competitor2: 25, score1: 17, score2: 6, turn: 7}
- {board: 20, competitor1: 18, competitor2: 33, score1: 0, score2: 12, turn: 7}
- {board: 21, competitor1: 2, competitor2: null, score1: 25, score2: 0, turn: 7}
modified: 2013-12-22 11:03:36
prealarm: 5
prized: true
prizefactor: '1'
prizes: millesimal
rankedturn: 7
season: 1
